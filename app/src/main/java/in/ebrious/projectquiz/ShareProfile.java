package in.ebrious.projectquiz;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.BuildConfig;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ShareProfile extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;

    ImageView share;
    Button shrbtn;
    TextView uname,wsc,dsc;
    private FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    LinearLayout linearLayout;
    CardView roundCardView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_profile);

        //for ad
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7270315510450456/4505028741");



        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

//        MobileAds.initialize(this, new OnInitializationCompleteListener() {
//            @Override
//            public void onInitializationComplete(InitializationStatus initializationStatus) {
//            }
//        });
//
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());


//        shrbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                } else {
//                    Log.d("TAG", "The interstitial wasn't loaded yet.");
//                }
//            }
//        });

//        mInterstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdClosed() {
//                // Load the next interstitial.
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
//            }
//
//        });
        share=(ImageView)findViewById(R.id.sharedp);
        shrbtn=(Button)findViewById(R.id.sharepr);
        uname=(TextView)findViewById(R.id.user_name);
        wsc=(TextView)findViewById(R.id.weekscr);
        dsc=(TextView)findViewById(R.id.dailyscr);
        linearLayout=(LinearLayout)findViewById(R.id.share_layout);
        roundCardView=(CardView)findViewById(R.id.roundCardView);

        shrbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                String shareBody = "Here is the share content body";
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//                startActivity(Intent.createChooser(sharingIntent, "Share via"));


                Share();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();


        mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                FirebaseUser user = mAuth.getCurrentUser();
                Uri photo=user.getPhotoUrl();

                //Users user1=snapshot.getValue(Users.class);
                Picasso.get().load(photo).into(share);
                for (DataSnapshot ds:snapshot.getChildren()){

                    //String data = ds.child("Email").getValue(String.class);
                    String data = snapshot.child("User Name").getValue(String.class);
                    //String email = snapshot.child("User Email").getValue(String.class);

                    //int val = Integer.valueOf((Integer) snapshot.child("User Score").getValue());

                    Integer val = snapshot.child("User Score").getValue(Integer.class);

                    //data=user.getEmail();
                    uname.setText(data);
                    //useremailid.setText(email);


                    int mba ;
                    if (val == null){

                    }
                    else {
                        mba = val;

                        String str = "Daily Score: "+mba;

                        dsc.setText(str);

                    }


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
//Display Weekend Score

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Weekend Score").child(mUid);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {



                for (DataSnapshot ds:snapshot.getChildren()){

                    //Integer val = snapshot.child("Weekend Score").child("High Score").getValue(Integer.class);
                    Integer val = snapshot.child("High Score").getValue(Integer.class);


                    String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();




                    if (val == null){

                    }
                    else {
                        int mba ;

                        mba = val;
                        String wscr = "Weekly Score: "+mba;
                        //Mainscore = mba;


                        wsc.setText(wscr);

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }

    private void Share() {
        Bitmap bitmap=getBitmapFromView(linearLayout);
        try {
            File file=new File(this.getExternalCacheDir(),"QuizBuck.jpeg");
            FileOutputStream fout=new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,fout);
            fout.flush();
            fout.close();
            file.setReadable(true,false);

            Intent intent=new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            intent.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(file));
            intent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(ShareProfile.this, BuildConfig.APPLICATION_ID + ".provider",file));
            intent.setType("image/jpeg");
            startActivity(Intent.createChooser(intent,"Share To"));

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }

//            if (mInterstitialAd.isLoaded()) {
//                mInterstitialAd.show();
//            } else {
//                Log.d("TAG", "The interstitial wasn't loaded yet.");
//            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    @SuppressLint("ResourceAsColor")
    private Bitmap getBitmapFromView(View view)
    {
        Bitmap returnedBitmap=Bitmap.createBitmap(view.getWidth(),view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas=new Canvas(returnedBitmap);
        Drawable bgdrawable=view.getBackground();
        if(bgdrawable != null)
        {
            bgdrawable.draw(canvas);
        }
        else
        {
            canvas.drawColor(android.R.color.white);
        }
        view.draw(canvas);

        return returnedBitmap;

    }


}
