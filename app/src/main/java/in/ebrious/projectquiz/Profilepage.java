package in.ebrious.projectquiz;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.google.android.gms.ads.InterstitialAd;


public class Profilepage extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;


    private FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    FirebaseDatabase database;

    ImageView pdp;
    TextView tvun, tvue, tvptm,weeksc,dailyscore;
    EditText etun, etue, etptm;
    Button editbtn, savebtn,signout,share,invite;
    private String CodeM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilepage);


        mAuth = FirebaseAuth.getInstance();

        pdp = (ImageView)findViewById(R.id.pdp_view);

        tvun = (TextView)findViewById(R.id.tvun);
        tvue = (TextView)findViewById(R.id.tvue);
        tvptm = (TextView)findViewById(R.id.tvptm);
        weeksc=(TextView)findViewById(R.id.weekscr);
        dailyscore=(TextView)findViewById(R.id.dailyscr);

//        etun = (EditText)findViewById(R.id.etun);
//        etue = (EditText)findViewById(R.id.etue);
        etptm = (EditText)findViewById(R.id.etptm);

        editbtn = (Button)findViewById(R.id.editbtn);
        savebtn = (Button)findViewById(R.id.savebtn);
        signout=(Button)findViewById(R.id.signoutbtn);
        share=(Button)findViewById(R.id.sharebtn);
        invite = (Button) findViewById(R.id.invitebtn);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7270315510450456/4505028741");

        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
//Display Weekend Score

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Weekend Score").child(mUid);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {



                for (DataSnapshot ds:snapshot.getChildren()){

                    //Integer val = snapshot.child("Weekend Score").child("High Score").getValue(Integer.class);
                    Integer val = snapshot.child("High Score").getValue(Integer.class);


                    String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();




                    if (val == null){

                    }
                    else {
                        int mba ;

                        mba = val;
                        String wscr = "Weekly Score: \n"+mba;
                        //Mainscore = mba;


                        weeksc.setText(wscr);

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mAuth.signOut();
                FirebaseAuth.getInstance().signOut();
                //mAuth.getInstance().signOut();



                Toast.makeText(Profilepage.this,"User Sign out succesfully",Toast.LENGTH_SHORT).show();
                Intent signout=new Intent(Profilepage.this,MainActivity.class);
                startActivity(signout);

            }
        });


        //for setting data in dp name and mail and display daily score
        FirebaseUser user = mAuth.getCurrentUser();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                FirebaseUser user = mAuth.getCurrentUser();
                Uri photo=user.getPhotoUrl();

                Picasso.get().load(photo).into(pdp);
                for (DataSnapshot ds:snapshot.getChildren()){

                    String data = snapshot.child("User Name").getValue(String.class);
                    String email = snapshot.child("User Email").getValue(String.class);
                    Integer val = snapshot.child("User Score").getValue(Integer.class);
                    String getpaytmnumber=snapshot.child("User Paytm").getValue(String.class);
                    CodeM = snapshot.child("User Code").getValue(String.class);

                    if(getpaytmnumber == null)
                    {
                        tvptm.setText(R.string.paytmnum);
                    }
                    tvptm.setText(getpaytmnumber);

                    if (val == null){

                    }
                    else {
                        int mba ;

                        mba = val;
                        String dscr = "Daily Score: \n"+mba;

                        dailyscore.setText(dscr);

                    }


                    tvun.setText(data);
                    tvue.setText(email);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        //btn for edit details and show edit text
        editbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvun.setVisibility(View.GONE);
                tvue.setVisibility(View.GONE);
                tvptm.setVisibility(View.GONE);


//                etun.setVisibility(View.VISIBLE);
//                etue.setVisibility(View.VISIBLE);
                etptm.setVisibility(View.VISIBLE);

                savebtn.setVisibility(View.VISIBLE);
                editbtn.setVisibility(View.GONE);

            }
        });

        //btn for save edited data
        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvun.setVisibility(View.VISIBLE);
                tvue.setVisibility(View.VISIBLE);
                tvptm.setVisibility(View.VISIBLE);
//                etun.setVisibility(View.GONE);
//                etue.setVisibility(View.GONE);

                String ptm= (String) tvptm.getText();
                String sptm= String.valueOf(etptm.getText());
                //CharSequence ptm=tvptm.getText();

                if(ptm != null)
                {
                    SavePtm(sptm);
                }
                etptm.setVisibility(View.GONE);

                savebtn.setVisibility(View.GONE);
                editbtn.setVisibility(View.VISIBLE);//GYUKGHJBJUH

            }
        });


        //share button
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent share=new Intent(Profilepage.this,ShareProfile.class);
                startActivity(share);
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });


        //invite button
        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = " Join and get 100 points using this refer code  " + CodeM +"  https://play.google.com/store/apps/details?id=in.ebrious.projectquiz&hl=en";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "SHARE");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));


            }
        });
    }

    private void SavePtm(String ptmnumber) {

        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("User").child(mUid).child("User Paytm");

        myRef.setValue(ptmnumber);
    }
}