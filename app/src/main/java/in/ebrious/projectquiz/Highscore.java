package in.ebrious.projectquiz;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

public class Highscore extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    ListView listView;
    ArrayList<String> aList = new ArrayList<>();
    String u, us; int h;
    ArrayAdapter<String> arrayAdapter;

    //ArrayList<String> aList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {


                for (DataSnapshot ds : snapshot.getChildren()) {


                    String data = snapshot.child("User Name").getValue(String.class);
                    us = data;

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference("Weekend Score");
//
        listView = findViewById(R.id.highscorelist);


        arrayAdapter= new ArrayAdapter<String>(Highscore.this ,android.R.layout.simple_list_item_1,aList);
        listView.setAdapter(arrayAdapter);

        mDatabase.orderByChild("High Score").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ds:snapshot.getChildren()) {

                            String userName = ds.child("User Name").getValue(String.class).toString();
                        Integer val = ds.child("High Score").getValue(Integer.class);
                        String highs = String.valueOf(val);
                        aList.add(userName +"\n" + highs);
                        //Collections.reverse(aList);


                        if (userName.equals(us))
                        {
                            u = userName+"\n"+highs;
                        }
                        arrayAdapter.notifyDataSetChanged();
                }
                Collections.reverse(aList);
                checkPosition();

             }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



}




    private void checkPosition() {


        int pos;
        pos = aList.indexOf(u) +1;
        Toast.makeText(Highscore.this, " Your Rank is: " + pos, Toast.LENGTH_LONG).show();


    }



}
