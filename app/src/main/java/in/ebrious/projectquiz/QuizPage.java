package in.ebrious.projectquiz;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Timer;
import java.util.TimerTask;

public class QuizPage extends AppCompatActivity {
    private AdView mAdView;

    TextView mTextField;
    ImageButton home, category;
    ProgressBar pb;
    Intent intent;
    int counter=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_page);

        mTextField =(TextView)findViewById(R.id.timertv);
        pb=(ProgressBar)findViewById(R.id.pb);
        home = (ImageButton) findViewById(R.id.homebtn);
        category = (ImageButton) findViewById(R.id.categorybtn);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //for home btn action
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(QuizPage.this, HomeScreen.class);
                startActivity(intent);
            }
        });

        //for category btn action
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(QuizPage.this, Category.class);
                startActivity(intent);

            }
        });

        //for timer text view
        new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTextField.setText(" " + (millisUntilFinished / 1000) + " ");
              //  pb.setProgress(30 - ((int) millisUntilFinished));
            }

            public void onFinish() {
                mTextField.setText("Time-Up!!");
            }
        }.start();

        prog();

    }

    //for progress bar
        private void prog() {

        pb=(ProgressBar)findViewById(R.id.pb);

        final Timer t = new Timer();
        TimerTask tt = new TimerTask() {
            @Override
            public void run() {

                counter++;
                pb.setProgress(counter);

                if (counter==100)
                    t.cancel();

            }
        };
        t.schedule(tt, 2, 200);
    }


}