package in.ebrious.projectquiz;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;


public class Redeem extends AppCompatActivity {
    TextView ptm,score,Refer;
    private FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    Button rdm,Apply;
    EditText ReferCode;
    Integer sc;
    String Ptn;
    FirebaseDatabase database;
    String refercode;

    private InterstitialAd mInterstitialAd;
    private int Sc;
    private String Code;
    ArrayList<String> aList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);
        ptm=(TextView)findViewById(R.id.ptm);
        score=(TextView)findViewById(R.id.tvscore);
        rdm=(Button)findViewById(R.id.redbtn);
        Refer=(TextView)findViewById(R.id.refertv);
        Apply=(Button)findViewById(R.id.applybtn);
        ReferCode=(EditText)findViewById(R.id.etcode);
        Apply.setEnabled(false);
        ReferCode.setEnabled(false);

        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();


        mDatabase = FirebaseDatabase.getInstance().getReference().child("User Reward").child(mUid);

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot ds:snapshot.getChildren()){

                    String user_reward = ds.child("User Code").getValue(String.class).toString();

                    aList.add(user_reward);
                  //  Toast.makeText(Redeem.this,"nillllllllllll",Toast.LENGTH_SHORT).show();
                    Toast.makeText(Redeem.this,user_reward+"",Toast.LENGTH_SHORT).show();

                    String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        Refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReferCode.setEnabled(true);

                //Toast.makeText(Redeem.this,refercode,Toast.LENGTH_SHORT).show();

            }
        });
        refercode=ReferCode.getText().toString();
        if(refercode!=null)
        {
            Apply.setEnabled(true);
        }
        else {
            Apply.setEnabled(false);
        }

        Apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        Code = snapshot.child("User Reward").getValue(String.class);
                        if (Code != null){

                            Toast.makeText(Redeem.this,"Already refered",Toast.LENGTH_SHORT).show();

                        }else if (Code == null){


//
//                            String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
//                            FirebaseDatabase database = FirebaseDatabase.getInstance();
//                            DatabaseReference myRefcode= database.getReference().child("User").child(mUid).child("User Reward");
//                            // refercode=ReferCode.getText().toString();
//                            //Toast.makeText(Redeem.this,refercode+"",Toast.LENGTH_SHORT).show();
//
//                            myRefcode.setValue("refercode");
//
//                            DatabaseReference ReScore = database.getReference().child("User").child(mUid).child("User Score");
//                            ReScore.setValue(sc+100);
//
//
//                            String mUid3 = FirebaseAuth.getInstance().getCurrentUser().getUid();
//
//                            mDatabase = FirebaseDatabase.getInstance().getReference().child("User Reward").child(mUid3);
//
//                            mDatabase.addValueEventListener(new ValueEventListener() {
//                                @Override
//                                public void onDataChange(@NonNull DataSnapshot snapshot) {
//
//
//                                    for (DataSnapshot ds:snapshot.getChildren()){
//
//
//                                        String user_reward=snapshot.child("User Code").getValue(String.class);
//
//                                       Toast.makeText(Redeem.this,user_reward+"",Toast.LENGTH_SHORT).show();
//
//
//                                        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
//
//
//                                    }
//                                }
//
//                                @Override
//                                public void onCancelled(@NonNull DatabaseError error) {
//
//                                }
//                            });
//


                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });


//                String mUid3 = FirebaseAuth.getInstance().getCurrentUser().getUid();
//
//                mDatabase = FirebaseDatabase.getInstance().getReference().child("User Reward").child(mUid3);
//
//                mDatabase.addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot snapshot) {
//
//
//                        for (DataSnapshot ds:snapshot.getChildren()){
//
//                            //Integer val = snapshot.child("User Score").getValue(Integer.class);
//                            String user_reward=snapshot.child("User Reward").getValue(String.class);
//
//                            Toast.makeText(Redeem.this,user_reward+"",Toast.LENGTH_SHORT).show();
//
//
//                            String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
//
////
//
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError error) {
//
//                    }
//                });
//



            }
        });



        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });



        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7270315510450456/4505028741");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        rdm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sc > 5000){
//                    Toast.makeText(Redeem.this,""+ptm.getText(),Toast.LENGTH_LONG).show();

                    if (ptm.getText() == ""){
                        Toast.makeText(Redeem.this,"Add your paytm number from profile page",Toast.LENGTH_LONG).show();

                    }
                    else {
                        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();


                        FirebaseDatabase database = FirebaseDatabase.getInstance();

                        DatabaseReference Pay = database.getReference().child("Request").child(mUid).child("Paytm Number");
                        DatabaseReference Scor = database.getReference().child("Request").child(mUid).child("Score");

                        Pay.setValue(Ptn);
                        Scor.setValue(sc);

                        Toast.makeText(Redeem.this,"You will receive your money in next 24 hours!",Toast.LENGTH_LONG).show();
                        Sc=sc-sc;

                        DatabaseReference NewScore = database.getReference().child("User").child(mUid).child("User Score");
                        NewScore.setValue(Sc);

                    }





                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    } else {
                        Log.d("TAG", "The interstitial wasn't loaded yet.");
                    }






                }
                else {

                    Toast.makeText(Redeem.this,"Sorry!! You don't have enough points to redeem, You need 5000 point to redeem",Toast.LENGTH_LONG).show();
                }
            }
        });


      //  String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(mUid);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {


                for (DataSnapshot ds:snapshot.getChildren()){

                    Integer val = snapshot.child("User Score").getValue(Integer.class);
                    String PtmNumber=snapshot.child("User Paytm").getValue(String.class);

                    ptm.setText(PtmNumber);


                    String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

                    sc = val;

                    Ptn = PtmNumber;


                    if (val == null){

                    }
                    else {
                        int mba ;

                        mba = val;
                        //Mainscore = mba;
                        //val=mba;

                        String str = "Score: "+mba;

                        score.setText(str);

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }
}