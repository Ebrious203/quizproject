package in.ebrious.projectquiz;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Questions extends AppCompatActivity implements View.OnClickListener, RewardedVideoAdListener {

    private AdView mAdView;

    TextView questions;
    TextView score;
    Button Reward;

    int qCount=0;
    Timer timer;
    int countdownValue = 26;
    int Mainscore=0;
    int nsc;


    RewardedAdLoadCallback adLoadCallback;

    private FirebaseAuth mAuth;


    CountDownTimer cdt;
    Button option1, option2, option3, option4,Notification;
    TextView mTextField;
    ImageButton home, category;
    ProgressBar pb;
    String ans;
    public static final int NOTIFICATION_ID = 1;

    int counter=0;
    int WeeklyPoints=0;

    DatabaseReference mDatabase;

    FirebaseDatabase database;
    DatabaseReference Ques;

    List<Questions> AllQuestion = new ArrayList<>();

    private int numberOfUsers;
    private double randomIndex;
    private String userkey;
    private RewardedVideoAd rewardedAd;


    String Username;
    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_page);

         mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7270315510450456/4505028741");

        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();


        LoadAdd();




        final MediaPlayer correct = MediaPlayer.create(this, R.raw.dingcorrect);
        final MediaPlayer wrong = MediaPlayer.create(this, R.raw.dingwrong);
        final Vibrator vibe = (Vibrator) Questions.this.getSystemService(Context.VIBRATOR_SERVICE);

        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Weekend Score").child(mUid);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {



                for (DataSnapshot ds:snapshot.getChildren()){

                    //Integer val = snapshot.child("Weekend Score").child("High Score").getValue(Integer.class);
                    Integer val = snapshot.child("High Score").getValue(Integer.class);


                    String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();




                    if (val == null){

                    }
                    else {
                        int mba ;

                        mba = val;
                        //String wscr = "Weekly Score: \n"+mba;
                        //Mainscore = mba;

                         WeeklyPoints=mba;
                         //Toast.makeText(Questions.this,WeeklyPoints+"",Toast.LENGTH_LONG).show();
                        //weeksc.setText(wscr);

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        String mUid2 = FirebaseAuth.getInstance().getCurrentUser().getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(mUid2);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                FirebaseUser user = mAuth.getCurrentUser();
                //Uri photo=user.getPhotoUrl();


                for (DataSnapshot ds:snapshot.getChildren()){

                    Integer val = snapshot.child("User Score").getValue(Integer.class);

                    String UsName = snapshot.child("User Name").getValue(String.class);

//                    Username = UsName;
                    String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    DatabaseReference myref3 = database.getReference().child("Weekend Score").child(mUid).child("User Name");
//                    Integer week= snapshot.child("Weekend Score").child("High Score").getValue(Integer.class);
//                    Toast.makeText(Questions.this,week+"",Toast.LENGTH_LONG).show();
                    myref3.setValue(UsName);

                    //nsc=val;

                    if (val == null){

                    }
                    else {
                        int mba ;
                        //WeeklyPoints=week;
                        mba = val;
                        Mainscore = mba;
                        //val=mba;


                        String str = "Score: "+mba;

                        score.setText(str);
                        if(val>=5000)
                        {
                            notificationDialog();

                        }


                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
//        Notification=(Button)findViewById(R.id.notify);
//        Notification.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //NotifyUser();
//                notificationDialog();
//            }
//        });

//        String scn= (String) score.getText();
//
//        if(nsc>=5000)
//        {
//            notificationDialog();
//
//        }



        questions = (TextView) findViewById(R.id.question);
        option1 = (Button) findViewById(R.id.op1);
        option2 = (Button) findViewById(R.id.op2);
        option3 = (Button) findViewById(R.id.op3);
        option4 = (Button) findViewById(R.id.op4);
        Reward=(Button)findViewById(R.id.reward);

       // MobileAds.initialize(this,"ca-app-pub-3940256099942544/5224354917");
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(Questions.this);

        loadRewardedVideoAd();


        Reward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mRewardedVideoAd.isLoaded()) {
                    mRewardedVideoAd.show();
                }

            }
        });



        score=(TextView) findViewById(R.id.score1);



        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(ans.equals("1"))
                {

                    Toast.makeText(Questions.this,"Yay!! Right Answer",Toast.LENGTH_SHORT).show();




                   // score.setText(String.valueOf(Mainscore));
                    saveScore(Mainscore + 1);
                    SaveweekScore(WeeklyPoints + 1);
                  //  fetchScore();


                    correct.start();
                    FetchNextQuestion();


                }
                else
                {

                    Toast.makeText(Questions.this,"Oops, Wrong Answer!!",Toast.LENGTH_SHORT).show();

                    wrong.start();
                    vibe.vibrate(80);

                    if (Mainscore == 0){

                        FetchNextQuestion();
                    }
                    else {



//
                        saveScore(Mainscore);
                        SaveweekScore(WeeklyPoints);

                        FetchNextQuestion();

                    }



                }
            }

        });

        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ans.equals("2"))
                {
                    Toast.makeText(Questions.this,"Yay! Right Answer",Toast.LENGTH_SHORT).show();

                    //WeeklyPoints=WeeklyPoints+2;
                    WeeklyPoints+=1;
                    Mainscore+=1;


                    saveScore(Mainscore);
                    SaveweekScore(WeeklyPoints);

//                    Toast.makeText(Questions.this,WeeklyPoints+"points",Toast.LENGTH_SHORT).show();

                    correct.start();

                    FetchNextQuestion();


                }
                else
                {
                    Toast.makeText(Questions.this,"Oops, Wrong Answer",Toast.LENGTH_SHORT).show();

                    wrong.start();
                    vibe.vibrate(80);

                    if (Mainscore == 0){

                        FetchNextQuestion();
                    }
                    else {


                        SaveweekScore(WeeklyPoints);
                        saveScore(Mainscore);

//                        Toast.makeText(Questions.this, WeeklyPoints + "points", Toast.LENGTH_SHORT).show();
                        FetchNextQuestion();

                    }


                }

            }
        });

        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ans.equals("3"))
                {
                    Toast.makeText(Questions.this,"Yay! Right Answer",Toast.LENGTH_SHORT).show();

                    //WeeklyPoints=WeeklyPoints+2;
                    WeeklyPoints+=1;
                    Mainscore+=1;

                   // score.setText(String.valueOf(Mainscore));
                   // fetchScore();
                    saveScore(Mainscore);

                    SaveweekScore(WeeklyPoints);

//                    Toast.makeText(Questions.this,WeeklyPoints+"points",Toast.LENGTH_SHORT).show();
                    correct.start();
                    FetchNextQuestion();


                }
                else
                {
                    Toast.makeText(Questions.this,"Oops, Wrong Answer",Toast.LENGTH_SHORT).show();
                    wrong.start();
                    vibe.vibrate(80);

                    if (Mainscore == 0){

                        FetchNextQuestion();
                    }
                    else {


                        //fetchScore();
                        SaveweekScore(WeeklyPoints);
                        saveScore(Mainscore);

//                        Toast.makeText(Questions.this, WeeklyPoints + "points", Toast.LENGTH_SHORT).show();
                        FetchNextQuestion();

                    }

                }

            }
        });

        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ans.equals("4"))
                {
                    Toast.makeText(Questions.this,"Yay! Right Answer",Toast.LENGTH_SHORT).show();

                    correct.start();

                    //WeeklyPoints=WeeklyPoints+2;
                    WeeklyPoints+=1;
                    Mainscore+=1;

                    //score.setText(String.valueOf(Mainscore));
                    //fetchScore();
                    saveScore(Mainscore);

                    SaveweekScore(WeeklyPoints);

//                    Toast.makeText(Questions.this,WeeklyPoints+"points",Toast.LENGTH_SHORT).show();
                    FetchNextQuestion();

                }
                else
                {
                    Toast.makeText(Questions.this,"Oops, Wrong Answer",Toast.LENGTH_SHORT).show();

                    wrong.start();
                    vibe.vibrate(80);

                    if (Mainscore == 0){

                        FetchNextQuestion();
                    }
                    else {


//                        Mainscore = Mainscore -2;
                        // DailyPoints = DailyPoints - 2;
                       // WeeklyPoints = WeeklyPoints - 2;

                        //score.setText(String.valueOf(Mainscore));
                        //fetchScore();
                        SaveweekScore(WeeklyPoints);
                        saveScore(Mainscore);

//                        Toast.makeText(Questions.this, WeeklyPoints + "points", Toast.LENGTH_SHORT).show();
                        FetchNextQuestion();

                    }

                }
            }
        });



        mTextField =(TextView)findViewById(R.id.timertv);
        pb=(ProgressBar)findViewById(R.id.pb);
        home = (ImageButton) findViewById(R.id.homebtn);
        category = (ImageButton) findViewById(R.id.categorybtn);


        //for home btn action
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Questions.this, HomeScreen.class);
                startActivity(i);
            }
        });

        //for category btn action
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Questions.this, Category.class);
                startActivity(i);

            }
        });


        Intent intent = getIntent();


        final String Cate = intent.getStringExtra("categ");

        database = FirebaseDatabase.getInstance();
//        Toast.makeText(Questions.this,Cate+"selected",Toast.LENGTH_SHORT).show();
       // Ques = database.getReference("Question Bank").child(Cate);





       FetchNextQuestion();


        //for timer text view

        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        int pbv = 26 - countdownValue;

                        pb.setProgress(pbv);

//                        countdownValue--;
                        --countdownValue;
                        mTextField.setText(" " + countdownValue);

                        //when the value of the countdown reaches 0, we start the result activity:
                        if (countdownValue == 0) {

                            FetchNextQuestion();
//
//                            timer.cancel();

                        }
                    }
                });
            }
        },0, 1000);

              //  Toast.makeText(Questions.this,ans,Toast.LENGTH_LONG).show();

            }

    private void notificationDialog() {
        NotificationManager notificationManager = (NotificationManager)       getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "tutorialspoint_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription("Sample Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                //.setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.quiz_buck)
                .setTicker("Tutorialspoint")
                //.setPriority(Notification.PRIORITY_MAX)
                .setContentTitle("QuizBuck")
                .setContentText("Your Reward is ready to redeem")
                .setContentInfo("Information");
        notificationManager.notify(1, notificationBuilder.build());
    }




//    private void NotifyUser() {
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(Questions.this)
//                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
//                .setContentTitle("Notify")
//                .setContentText("Now you can redeem your points");
//
//        Intent notification=new Intent(this,Questions.class);
//      //  PendingIntent contentintent=PendingIntent.getActivity(this,0,notification,PendingIntent.FLAG_UPDATE_CURRENT);
//        PendingIntent contentintent = PendingIntent.getActivity(this,0,notification,PendingIntent.FLAG_UPDATE_CURRENT);
//        builder.setContentIntent(contentintent);
//        Toast.makeText(this,"Kai bi",Toast.LENGTH_LONG).show();
//        NotificationManager manager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//        manager.notify(0,builder.build());
//
//    }

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd("ca-app-pub-7270315510450456/7470312863",
                new AdRequest.Builder().build());
    }

    //





    //}

    //@Override
//    public void onRewardedAdClosed() {
//        this.rewardedAd = createAndLoadRewardedAd();
//    }

    private void LoadAdd() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    private void saveScore(int m) {

        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("User").child(mUid).child("User Score");

        myRef.setValue(m);

    }

    private void SaveweekScore(int n) {

        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
       // String data = database.getReference().child("User Name").getValue(String.class);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myref2 = database.getReference().child("Weekend Score").child(mUid).child("High Score");
        //Toast.makeText(Questions.this,n+"",Toast.LENGTH_LONG).show();



        myref2.setValue(n);
//ahiyaaaaaaa


    }





    @Override
    public void onClick(View view) {

    }
    public void FetchNextQuestion()
    {

        qCount++;
        
        if (qCount == 10)
        {

            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                Log.d("tag","Loaded");
            }
            else {
                Log.d("tag","not Loaded");
            }
            
            //for ad
            
            qCount=0;
        }
        
//        rewardedAd.loadAd("ca-app-pub-7270315510450456/7470312863", new AdRequest.Builder().build());
      //  LoadAdd();

     //  rewardedAd.loadAd("ca-app-pub-7270315510450456/7470312863", new AdRequest.Builder().build());

        option1.setEnabled(false);
        option1.postDelayed(new Runnable() {
            @Override
            public void run() {
                option1.setEnabled(true);
            }
        }, 6000);

        option2.setEnabled(false);
        option2.postDelayed(new Runnable() {
            @Override
            public void run() {
                option2.setEnabled(true);
            }
        }, 6000);
        option3.setEnabled(false);
        option3.postDelayed(new Runnable() {
            @Override
            public void run() {
                option3.setEnabled(true);
            }
        }, 6000);
        option4.setEnabled(false);
        option4.postDelayed(new Runnable() {
            @Override
            public void run() {
                option4.setEnabled(true);
            }
        }, 6000);

         numberOfUsers = 150;

         randomIndex = Math.floor(Math.random()* numberOfUsers);

         if (randomIndex <= 0){
             FetchNextQuestion();
         }
         else {



             countdownValue = 26;
             Intent intent = getIntent();

             final String Cate = intent.getStringExtra("categ");

             mDatabase = FirebaseDatabase.getInstance().getReference().child("Question Bank").child(Cate);


             mDatabase.limitToFirst((int) randomIndex).addValueEventListener(new ValueEventListener() {
                 @Override public void onDataChange(@NonNull DataSnapshot snapshot) {


                     for (DataSnapshot ds:snapshot.getChildren()){



                         String amount = ds.child("Question").getValue().toString();
                         String opt1=ds.child("Option 1").getValue().toString();
                         String opt2=ds.child("Option 2").getValue().toString();
                         String opt3=ds.child("Option 3").getValue().toString();
                         String opt4=ds.child("Option 4").getValue().toString();
                         ans=  ds.child("Right Ans").getValue().toString();



                         Log.i("Tag",amount+"");
                         //Toast.makeText(Questions.this,ans+"is the answer",Toast.LENGTH_SHORT).show();

                         questions.setText(amount);
                         option1.setText(opt1);
                         option2.setText(opt2);
                         option3.setText(opt3);
                         option4.setText(opt4);

                     }

                 }

                 @Override
                 public void onCancelled(@NonNull DatabaseError error) {

                 }
             });
         }


    }

    @Override
    public void onRewardedVideoAdLoaded() {

      //  Toast.makeText(this, "onRewarded! currency: ", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRewardedVideoAdOpened() {
    //    Toast.makeText(this, "onRewarded! currency: ", Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onRewardedVideoStarted() {

      //  Toast.makeText(this, "onRewarded! currency: ", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRewardedVideoAdClosed() {

      //  Toast.makeText(this, "onRewarded! currency: ", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {

        Toast.makeText(this, "The Option number " + ans + " is a right Ans.", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
      //  Toast.makeText(this, "onRewarded! currency: ", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        //Toast.makeText(this, "Failed! currency: ", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRewardedVideoCompleted() {
        Toast.makeText(this, "The Option number " + ans + " is a right Ans.", Toast.LENGTH_LONG).show();
    }
}