package in.ebrious.projectquiz;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Random;

public class HomeScreen extends AppCompatActivity {

    private AdView mAdView;

    private FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    private InterstitialAd mInterstitialAd;
    LinearLayout profile;
    String Code;

    ImageView userp;
    TextView tvuser,useremailid,showscore,UserCode;
    Button play, highscr, redeem, rule,usercode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
//        UserCode.setText(generateCode(6));

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {


            }


        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7270315510450456/4505028741");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });


        //for net
        if (!isCon(HomeScreen.this))
        {
            builderDialouge(HomeScreen.this).show();
        }
        else
        {
            setContentView(R.layout.activity_home_screen);
        }

        mAuth = FirebaseAuth.getInstance();

        tvuser=(TextView)findViewById(R.id.username);
        useremailid=(TextView)findViewById(R.id.userMail);
        userp=(ImageView)findViewById(R.id.dp_view);
        play=(Button)findViewById(R.id.playbtn);
        showscore=(TextView)findViewById(R.id.sc);
        //UserCode=(TextView)findViewById(R.id.tvcode1);

        highscr = (Button)findViewById(R.id.highscrbtn);
        redeem =  (Button)findViewById(R.id.redeembtn);
        rule = (Button)findViewById(R.id.rulebtn);
//        usercode=(Button)findViewById(R.id.code);
//        UserCode.setText(generateCode(6));



//        usercode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //UserCode.setText(generateCode(6));
//            }
//        });


        profile = (LinearLayout)findViewById(R.id.profile_layout);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
                Intent playgame=new Intent(HomeScreen.this,Category.class);
                startActivity(playgame);
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeScreen.this, Profilepage.class);
                startActivity(i);
            }
        });
        highscr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeScreen.this, Highscore.class);
                startActivity(i);
            }
        });

        redeem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {

                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

                Intent i = new Intent(HomeScreen.this, Redeem.class);
                startActivity(i);
            }
        });
        rule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(HomeScreen.this, Rules.class);
                startActivity(i);
            }
        });

        FirebaseUser user = mAuth.getCurrentUser();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUid());

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                FirebaseUser user = mAuth.getCurrentUser();
                Uri photo=user.getPhotoUrl();

                //Users user1=snapshot.getValue(Users.class);
                Picasso.get().load(photo).into(userp);


                    //String data = ds.child("Email").getValue(String.class);
                    String data = snapshot.child("User Name").getValue(String.class);
                    String email = snapshot.child("User Email").getValue(String.class);

                    Code = snapshot.child("User Code").getValue(String.class);

                    if (Code == null) {


                    Toast.makeText(HomeScreen.this, "Okay", Toast.LENGTH_LONG).show();
                    generateCode(6);

                }
                    else if (Code != null ){
                        Toast.makeText(HomeScreen.this,"Code is already generated",Toast.LENGTH_SHORT).show();
                    }
                    //int val = Integer.valueOf((Integer) snapshot.child("User Score").getValue());

                    Integer val = snapshot.child("User Score").getValue(Integer.class);

                    //data=user.getEmail();
                    tvuser.setText(data);
                    useremailid.setText(email);


                    int mba ;
                    if (val == null){

                    }
                    else {
                        mba = val;

                        String str = "Daily Score: "+mba;

                        showscore.setText(str);

                    }












//               // String email = snapshot.child(user.getUid()).child("Email").getValue(String.class);
//
//                String Email = String.valueOf(snapshot.child(user.getUid()).child("Email").getValue(String.class));
//          //     String Email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
//               tvuser.setText(Email);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
//
//        if (Code == null){
//
//            Toast.makeText(this,"Okay",Toast.LENGTH_LONG).show();
//            generateCode(6);
//
//        }else {
//
//
//            Toast.makeText(this,Code+"nooo",Toast.LENGTH_LONG).show();
//        }


    }

    private String generateCode(int length) {

        char [] chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
        StringBuilder stringBuilder=new StringBuilder();
        Random random=new Random();
        for(int i=0;i<length;i++)
        {
            char c=chars[random.nextInt(chars.length)];
            stringBuilder.append(c);

        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String mUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        DatabaseReference MyCode = database.getReference().child("User").child(mUid).child("User Code");

        DatabaseReference myref23 = database.getReference().child("User Reward").child("User Code").child(mUid).child("ReferCode");

        MyCode.setValue(stringBuilder.toString());
        myref23.setValue(stringBuilder.toString());


        return stringBuilder.toString();
    }


    //for internet connection

    public boolean isCon(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo  = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting())
        {

            android.net.NetworkInfo wifi  = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile  = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) ||
                    (wifi != null && wifi.isConnectedOrConnecting()) )
            {
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    public AlertDialog.Builder builderDialouge(Context c)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);

        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or WiFi Connection To Play!");

        builder.setCancelable(false);

        builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(HomeScreen.this, HomeScreen.class);
                startActivity(intent);
//                finish();

            }
        });

        return builder;

    }


}