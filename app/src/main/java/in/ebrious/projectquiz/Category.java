package in.ebrious.projectquiz;

import android.animation.ArgbEvaluator;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.List;

public class Category extends AppCompatActivity {

    private AdView mAdView;

    ViewPager viewPager;
    Adapter adapter;
    List<Model> models;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        models = new ArrayList<>();
        models.add(new Model(R.drawable.knowledge, "General knowledge", "General knowledge is important for enhancing our sense of perceiving the world, for better understanding, and analysis of the situations cannot be possible without proper knowledge."));
        models.add(new Model(R.drawable.history, "Indian History", "India is one of the oldest civilizations in the world with a kaleidoscopic variety and rich cultural heritage. It has achieved all-round socio-economic progress since Independence."));
        //models.add(new Model(R.drawable.tv, "T.V. and Films", "India’s film and television industry is one of the fastest growing sectors in the country today and has attracted significant investments from Indian and international corporates."));
        models.add(new Model(R.drawable.hindi, "Hindi question", "Business cards are cards bearing business information about a company or individual."));
        models.add(new Model(R.drawable.sport, "Sports", "Business cards are cards bearing business information about a company or individual."));
        models.add(new Model(R.drawable.quiz, "2019 quiz", "A general knowledge public quiz all about 2019."));
        models.add(new Model(R.drawable.quiz, "2020 Quiz", "A general knowledge public quiz all about 2020."));
        models.add(new Model(R.drawable.techn, "Technologies", "Technology is the study and transformation of techniques, tools, and machines created by humans. Technology allows humans to study and evolve the physical elements that are present in their lives."));
        models.add(new Model(R.drawable.science, "Science", "Science quiz is basically set of question relating to topics in science subject which can be answered with short statements or by choosing between alternative answers.. These question may be put to set of individual or group of individuals in a public forum as some kind of contest between them."));
        models.add(new Model(R.drawable.math, "Mathematics", "Mathematics makes our life orderly and prevents chaos. Certain qualities that are nurtured by mathematics are power of reasoning, creativity, abstract or spatial thinking, critical thinking, problem-solving ability and even effective communication skills.."));
        models.add(new Model(R.drawable.politics, "Politics", "Politics is the way that people living in groups make decisions. Politics is about making agreements between people so that they can live together in groups such as tribes, cities, or countries. ... "));



        adapter = new Adapter(models, this);

        viewPager = findViewById(R.id.ViewPager);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(130, 0, 130, 0);

        Integer[] colors_temp = {
                getResources().getColor(R.color.color1),
                getResources().getColor(R.color.color2),
                getResources().getColor(R.color.color3),
                getResources().getColor(R.color.color4),
                getResources().getColor(R.color.color5),
                getResources().getColor(R.color.color6),
                getResources().getColor(R.color.color7),
                getResources().getColor(R.color.color8),
                getResources().getColor(R.color.color9),
                getResources().getColor(R.color.color10),
                getResources().getColor(R.color.color11),

        };

        colors = colors_temp;

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position < (adapter.getCount() -1) && position < (colors.length - 1)) {
                    viewPager.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    colors[position],
                                    colors[position + 1]
                            )
                    );
                }

                else {
                    viewPager.setBackgroundColor(colors[colors.length - 1]);
                }
            }

            @Override
            public void onPageSelected(int position) {
//                String cate = models.get(position).getTitle();
//                Intent intent=new Intent(Category.this,Questions.class);
//                intent.putExtra("cat",cate);
//                startActivity(intent);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}